#include <iostream>
#include <cstdio>
using namespace std;

int max_of_four(int a,int b,int c,int d)
{
    int numbers[4]={a,b,c,d};
    int max=numbers[0];
    for(int x=1;x<4;x++)
    {
        if(numbers[x]>max)
        max=numbers[x];
    }
    return max;
}

int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);
    
    return 0;
}