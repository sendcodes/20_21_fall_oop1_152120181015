#include<iostream>
#include<stdlib.h>
#include<fstream>

using namespace std;
/**
* summary of all terms
* @param double *p
*/
void sum(double *p)
{
	int sum = 0;
	for (int i = 1; i < p[0]+1; i++)
	{
		sum = sum + p[i];
	}
	cout<< "sum: "<< sum<<endl;
}
/**
* multiplication of all terms
* @param double *x
*/
void mul(double *p)
{
	int mul = 1;
	for (int i = 1; i < p[0]+1; i++)
	{
		mul = mul *p[i] ;
	}
	cout << "mul: " << mul<<endl;
}
/**
* average value of all terms
* @param double *x
*/
void avg(double *p)
{
	float sum = 0,avg;
	for (int i = 1; i < p[0]+1; i++)
	{
		sum = sum +p[i] ;
	}
	avg = sum / p[0];
	cout << "avg: " << avg<<endl;
}
/**
* finding the smallest value
* @param double *x
*/
void small(double *p)
{
	double min = p[1];
	for (int i = 1; i < p[0]; i++)
	{
		if (p[i+1] < p[i])
		{
			min = p[i+1];
		}
	}
	cout << "smallest term: " << min << endl;
}
void main()
{
	double * x;
	x = new double [10];
	fstream file("input.txt", ios::in);
	file >> x[0];
	for (int i = 0 ; i < x[0] ;i++)
	{
			file >> x[i+1];
	}
	for (int i = 0; i < x[0]+1; i++)
	{
		cout << x[i] << endl;
	}
	sum(x);
	mul(x);
	avg(x);
	small(x);
	delete[] x;
	file.close();
	system("pause");
}